package com.zstudiolabs.listview;

import android.app.ListFragment;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new ListViewFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ListViewFragment extends ListFragment {

        public ListViewFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            ArrayList<LocationsModel> locationsArray = LocationsController.sharedInstance(getActivity()).getLocationsModels();

            LocationsAdapter arrayAdapter = new LocationsAdapter(locationsArray);
            setListAdapter(arrayAdapter);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);


            return rootView;
        }

        private class LocationsAdapter extends ArrayAdapter<LocationsModel>
        {
            public LocationsAdapter(ArrayList<LocationsModel> locations)
            {
                super(getActivity(), 0, locations);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if( convertView == null ){
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item, null);
                    Log.i("TAG", "Created new convertView");
                }

                Log.i("TAG", "Setting up row for position: " + position);

                LocationsModel currentLocationItem = getItem(position);

                TextView title = (TextView) convertView.findViewById(R.id.nameTextView);
                title.setText(currentLocationItem.getLocationName());

                TextView descriptionView = (TextView) convertView.findViewById(R.id.descriptionTextView);
                descriptionView.setText(currentLocationItem.getLocationDescription());

                CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.checkBox);
                checkBox.setChecked(  position%2 == 0 );
                
                return convertView;
            }
        }
    }
}
