package com.zstudiolabs.databases;

import android.provider.BaseColumns;

/**
 * Created by michael.ziray on 3/3/15.
 */
public final class LocationsContract
{
    public LocationsContract(){}

    public static abstract class LocationsEntry implements BaseColumns
    {
        /* LOCATIONS TABLE */
        public static final String TABLE_LOCATIONS      = "Locations";
        public static final String COLUMN_LOCATION_NAME = "location_name";
        public static final String COLUMN_LOCATION_ID   = "location_id";
        public static final String COLUMN_DESCRIPTION   = "description";


        /* COORDINATES TABLE */
        public static final String TABLE_COORDINATES     = "Coordinates";
        public static final String COLUMN_COORDINATES_ID = "id";
        public static final String COLUMN_LATITUDE       = "latitude";
        public static final String COLUMN_LONGITUDE      = "longitude";
    }
}
