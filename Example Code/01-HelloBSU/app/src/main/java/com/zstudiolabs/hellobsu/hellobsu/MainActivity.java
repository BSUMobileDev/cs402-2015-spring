package com.zstudiolabs.hellobsu.hellobsu;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    Button launchButton;

    public static final String STRING_DATA = "STRING_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("EXAMPLE", "CREATED ###################################");

        setupInterface();


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("EXAMPLE", "START AGAIN ------------------------------");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.class.toString(), "RESUME AGAIN -----------------------------");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("EXAMPLE", "PAUSE AGAIN ------------------------------");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("EXAMPLE", "STOP AGAIN ------------------------------");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("EXAMPLE", "Destroyed AGAIN !!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    private void setupInterface() {
        launchButton = (Button)findViewById(R.id.button);

        launchButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( MainActivity.this, "This is a toast", Toast.LENGTH_LONG).show();

                Intent secondActivityIntent = new Intent( MainActivity.this, SecondActivity.class);
                secondActivityIntent.putExtra(STRING_DATA, "data from the first activity");
                startActivityForResult(secondActivityIntent, 0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String returnData = data.getStringExtra(SecondActivity.RETURN_DATA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
